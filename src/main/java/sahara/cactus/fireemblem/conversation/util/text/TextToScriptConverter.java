/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.text;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;

/**
 * Converts game text to a script.
 * <p>
 * Unrecognized control codes will be left untouched, and will not prevent the
 * script from being displayed in the viewer, though those codes will not have
 * their effects recognized. They will, however, function as normal once
 * resaved and imported into the game.
 * 
 * @author Secretive Cactus
 */
public class TextToScriptConverter {
  
  /**
   * Matches a command to play a BGM.
   * <p>
   * Format: $Sbp(Name)|(Offset)|
   */
  private static final Pattern BGM_PATTERN = Pattern.compile(
                                                             // Match Name
                                                             "\\$Sbp(\\w+)\\|" + 
                                                                 // Match Offset
                                                                     "(\\d+)\\|"
                                                     );
  
  /**
   * Matches a command to set the display type.
   * <p>
   * Format: $t(Type)
   */
  private static final Pattern TYPE_PATTERN = Pattern.compile(
                                                                   // Match Type
                                                                     "\\$t(\\d)"
                                                      );
  
  /**
   * Matches a command to display a character bust.
   * <p>
   * Format: $Wm(Name)|(Position){$w0\|}?{\|Emotions}?|
   * <p>
   * Emotions Format: {$Ws\1|}?$Wa$E(Emotion)
   * <p>
   * Position varies based on the display type. For type 1, it should be 3 for
   * left or 7 for right. For type 0, it should be 0 for left or 6 for right.
   * <p>
   * $w0| can be pushed back to the end of a chain of control codes.
   */
  private static final Pattern ENTER_PATTERN = Pattern.compile(
                                                           // Match Name
                                                           "\\$Wm([^\\|]+)\\|" +
                                                               // Match Position
                                                                       "(\\d)" +
                                                                  // Match $w0
                                                                  "(?:\\$w0)?" +
                                                      // Match Previous Name
                                                      "(?:\\|(?:\\$Ws\\1\\|)?" +
                                                // Match Emotions
                                                "\\$Wa\\$E([^\\|\r\n]+))?\\|?");
  
  /**
   * Matches a command to display text. Will continue to match for as long as a
   * second character does not begin speaking and a second sound effect is not
   * encountered.
   * <p>
   * Format: $Ws(Name)|{(Modifiers)}?{$Wa}?{{{^\r\n]+}\n$k$p(Modifiers)}+}?
   * <p>
   * Modifiers Format: {${\w+}(Value)\|}+
   */
  private static final Pattern SPEAKING_PATTERN = Pattern.compile(
                                                     // Match Name
                                                     "\\$Ws([^\\|\\r\\n]+)\\|" +
                                                    // Match All Modifiers
                                                    "((?:[^\\|\\r\\n]+\\|)+)?" +
                                                   // Match $Wa for no Modifiers
                                                                  "(?:\\$Wa)?" +
                                    // Match everything up to a new line of text
                                                  "((?:[^\\r\\n]+?\\n\\$k\\$p" +
                                              // Match modifiers on the new line
                                                "(?:(?:[^\\|\\r\\n]+\\|)+)?)+)?"
                                                  );
  
  /**
   * Matches a command to hide a character bust.
   * <p>
   * Format: {$k{$p|\n}}?{$Ws(Name)\|}?$Wd{$w0\|}?
   * <p>
   * $w0| can be pushed back to the end of a chain of control codes.
   */
  private static final Pattern EXIT_PATTERN = Pattern.compile(
                                                    // Potentially match newline
                                                     "(?:\\$k(?:\\$p|\\\\n))?" +
                                                  // Potentially match Name
                                                  "(?:\\$Ws([^\\|\r\n]+)\\|)?" +
                                                                    // Match $Wd
                                                                       "\\$Wd" +
                                                       // Potentially match $w0|
                                                               "(?:\\$w0\\|)?");
  
  /**
   * Matches a command to change a character bust's expression.
   * <p>
   * Format: {$Ws(Name)\||$k$p}?{$Wa}?$E(Emotions)|{\n}?
   */
  private static final Pattern EMOTION_CHANGE_PATTERN = Pattern.compile(
                                                    // Potentially match Name
                                                    "(?:\\$Ws([^\\|\r\n]+)\\|" +
                                                    // Alternately match newline
                                                                 "|\\$k\\$p)?" +
                                                        // Potentially match $Wa
                                                                  "(?:\\$Wa)?" +
                                                        // Match Emotions
                                                        "\\$E([^\\|\r\n]+)\\|" + 
                                                    // Potentially match newline
                                                                         "\n?");
  
  private static final Pattern SIMPLE_EMOTION_CHANGE_PATTERN = Pattern.compile(
                                                            "\\$E(.+?,[照汗]*)");
  
  /**
   * Matches a command to play a sound effect.
   * <p>
   * Format: {$k$p}?{$Wa}?$Svp(Sound Effect)|
   */
  private static final Pattern SOUND_EFFECT_PATTERN = Pattern.compile(
                                                    // Potentially match newline
                                                               "(?:\\$k\\$p)?" +
                                                        // Potentially match $Wa
                                                                  "(?:\\$Wa)?" +
                                                      // Match Sound Effect
                                                      "\\$Svp([^\\|\r\n]+)\\|");
  
  /**
   * Matches a command to active the use of $Nu in Fire Emblem Fates.
   * <p>
   * Format: $a
   */
  private static final Pattern PERMANENT_PATTERN = Pattern.compile("\\$a");
  
  /**
   * Matches a command to display variant text based on the player's gender.
   * <p>
   * Format: $G(Male Text),(Female text)|
   */
  private static final Pattern G_PATTERN = Pattern.compile(
                                                     // Matches the full command
                                                      "(\\$G[^,]+,[^\\|]+\\|)");
  
  /**
   * Used to extract and replace instances of G_PATTERN before and after other
   * processing is performed. Important for avoiding collisions in some edge
   * cases.
   */
  private static final Pattern INSG_PATTERN = Pattern.compile("_I_N_S_G_");
  
  /**
   * Matches lines that should be skipped when processing speaking patterns.
   * <p>
   * Format: {$Svp|\|$Wm}
   */
  private static final Pattern SKIP_MODIFIERS = Pattern.compile(
                                                        // Matches $Svp or |$Wm 
                                                        "^(?:\\$Svp|\\|\\$Wm)");
  
  /**
   * The formats used for converting game text enter codes to script commands.
   */
  private static final String[] ENTER_FORMAT = new String[] {
    "%s%s, %s)\n%s", "%s%s)\n$Ws%s|$Wa$E%s|", "%s%s)\n", "%s%s, %s)\n"
                                               };
  
  /**
   * The format used for converting game text exit codes to script commands.
   */
  private static String EXIT_FORMAT;
  
  /**
   * The format used for converting game text BGM codes to script commands.
   */
  private static String BGM_FORMAT;
  
  /**
   * The format used for converting game text speech to script commands.
   */
  private static final String SPEAKING_FORMAT = "%s%s%s%s%s%s: %s%s%s";
  
  /**
   * The format used for converting game text emotion codes to script commands.
   */
  private static String EMOTION_FORMAT;
  
  /**
   * The format used for converting game text emotion codes with attached names
   * to script commands.
   */
  private static String NAMED_EMOTION_FORMAT;
  
  /**
   * The format used for converting game text sound effect codes to script
   * commands.
   */
  private static String SOUND_EFFECT_FORMAT;
  
  /**
   * Matches instances of assigning a character speech command to a character
   * entry command. Used solely for direct replacement with a character entry
   * command.
   * <p>
   * Format: $Ws{[^\|]+}|$Wa$Wm
   * <p>
   * This has been included for convenience as well as the potential for edge
   * cases. While none of the default game files that have been loaded into the
   * editor required this, at least one file from the Awakening gay mod requires
   * this in order to be read correctly. As the possibility remains that some
   * file somewhere in one of the games would also require this, it was added.
   */
  private static final Pattern INVALID_CHARACTER_REFERENCE = Pattern.compile(
                                        // Matches the entire problematic string
                                                   "\\$Ws[^\\|]+\\|\\$Wa\\$Wm");
  
  /**
   * Replaces instances where a speech command is incorrectly placed before an
   * emotion shift.
   * <p>
   * Format: \n([^\r\n]+?) \(([^\r\n\)]+)\): \n
   * <p>
   * Replacement: \n(EMOTION SHIFT: $1, $2)\n
   */
  private static SubstitutionPair EMOTION_SUBSTITUTION;
  
  /**
   * Contains substitutions for a number of simple game text patterns, largely
   * related to line breaks.
   */
  private static final SubstitutionPair[] FINAL_SUBSTITUTIONS =
                                                        new SubstitutionPair[] {
    null,
    new SubstitutionPair(Pattern.quote("\\n"), "\n"),
    new SubstitutionPair("[\r\n]+\\$k[\r\n]+", "\n"),
    new SubstitutionPair("\n\n", "\n"),
    new SubstitutionPair("([^\\|])\\$w0([^\\|])", "$1$2"),
    new SubstitutionPair(Pattern.quote("$k$p"), ""),
  };
  
  /**
   * Whether or not the converter is finished running.
   */
  private boolean finished;
  
  /**
   * The extracted gender substitutions.
   */
  private ArrayList<String> gValues;
  
  /**
   * The most recent line match.
   */
  private Matcher match;
  
  /**
   * The converted text.
   */
  private String text;
  
  /**
   * The display type of the text.
   */
  private int type;
  
  /**
   * Creates a game text converter for the given game text.
   * 
   * @param text the text that the converter will convert
   */
  public TextToScriptConverter(String text) {
    if (text.startsWith("[" + XMLManager.getText("Script_Command_Script")) ||
        text.startsWith("[" + XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
      this.text = text;
      finished = true;
    } else {
      this.text = text;
      finished = false;
      gValues = new ArrayList<>();
      type = 1;
    }
  }
  
  /**
   * Returns whether or not the text has been converted.
   * 
   * @return whether or not the text has been converted
   */
  public boolean isFinished() {
    return finished;
  }
  
  /**
   * Returns the most recent line match.
   * 
   * @return the most recent line match
   */
  public Matcher getMatch() {
    return match;
  }
  
  /**
   * Returns the converter's text.
   * 
   * @return the converter's text
   */
  public String getText() {
    return text;
  }
  
  /**
   * Returns the display type of the converter's text.
   * 
   * @return the display type of the converter's text
   */
  public int getType() {
    return type;
  }
  
  /**
   * Converts an emotion from standard game text to script format.
   * 
   * @param text the game text containing the emotion
   * @return the emotion in script format
   */
  private String processEmotion(String text) {
    int i;
    String emotion;
    if (text.startsWith(":")) {
      text = text.substring(1);
    }
    if (text.contains(";")) {
      text = text.replace(";", ",");
    }
    if (text.contains("、")) {
      text = text.replace(";", ",");
    }
    if (text.startsWith("照汗,")) {
      text = "通常,照汗";
    } else if (text.startsWith("苦汗")) {
      text = text.replace("苦汗", "苦,汗");
    }
    text = text.replaceAll("\\s+", "");
    if ((i = text.indexOf(",")) > -1) {
      emotion = ScriptHelper.EMOTION_JA_AS_TR.get(text.substring(0, i));
      if (text.contains("照")) {
        emotion += ", " + XMLManager.getText("Emotion_Blushing");
      }
      if (text.contains("汗")) {
        emotion += ", " + XMLManager.getText("Emotion_Sweat");
      }
    } else {
      emotion = ScriptHelper.EMOTION_JA_AS_TR.get(text);
    }
    return emotion;
  }
  
  /**
   * Extracts and saves the script's gender substitutions.
   */
  private void extractGenderSubstitutions() {
    match = G_PATTERN.matcher(text);
    while (match.find()) {
      gValues.add(match.group(1));
      text = text.replaceFirst(Pattern.quote(match.group(0)), "_I_N_S_G_");
    }
  }
  
  /**
   * Loads all of the ScriptHelper's patterns for the current editor language.
   */
  public static void reloadPatterns() {
    EXIT_FORMAT = "(" + XMLManager.getText("Script_Command_Exit") + ": %s)\n";
    BGM_FORMAT = "(" + XMLManager.getText("Script_Command_BGM") + ": %s, %s)\n";
    SOUND_EFFECT_FORMAT = "(" +
                          XMLManager.getText("Script_Command_Sound_Effect") +
                          ": %s)\n";
    EMOTION_FORMAT = "(" + XMLManager.getText("Script_Command_Emotion_Shift") +
                     ": %s)\n";
    NAMED_EMOTION_FORMAT = "(" +
                           XMLManager.getText("Script_Command_Emotion_Shift") +
                           ": %s, %s)\n";
    EMOTION_SUBSTITUTION = new SubstitutionPair(
                                        "\n([^\r\n]+?) \\(([^\r\n\\)]+)\\): \n",
                    "\n(" + XMLManager.getText("Script_Command_Emotion_Shift") +
                                                ": $1, $2)\n");
    FINAL_SUBSTITUTIONS[0] = new SubstitutionPair(
                                          "\\$c(\\d+),(\\d+),(\\d+),(\\d+)\\|?",
                       "\n(" + XMLManager.getText("Script_Command_Text_Color") +
                                                  ": $1, $2, $3, $4)\n");
  }
  
  /**
   * Replaces BGM patterns with script commands.
   */
  private void replaceBGMPatterns() {
    match = BGM_PATTERN.matcher(text);
    while (match.find()) {
      text = text.replaceFirst(Pattern.quote(match.group(0)),
                              Matcher.quoteReplacement(String.format(BGM_FORMAT,
                                              match.group(1), match.group(2))));
    }
  }
  
  /**
   * Replaces emotion patterns with script commands.
   */
  private void replaceEmotionPatterns() {
    match = EMOTION_CHANGE_PATTERN.matcher(text);
    while (match.find()) {
      String emotion = processEmotion(match.group(2));
      if (match.group(1) != null) {
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                    Matcher.quoteReplacement(String.format(NAMED_EMOTION_FORMAT,
                 CharacterManager.getTranslatedName(match.group(1)), emotion)));
      } else {
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                          Matcher.quoteReplacement(String.format(EMOTION_FORMAT,
                                                                 emotion)));
      }
    }
    text = EMOTION_SUBSTITUTION.replaceAll(text);
    match = SIMPLE_EMOTION_CHANGE_PATTERN.matcher(text);
    while (match.find()) {
      String emotion = processEmotion(match.group(1));
      text = text.replaceFirst(Pattern.quote(match.group(0)),
                   Matcher.quoteReplacement("\n" + String.format(EMOTION_FORMAT,
                                                                 emotion)));
    }
  }
  
  /**
   * Replaces enter patterns with script commands.
   */
  private void replaceEnterPatterns() {
    match = ENTER_PATTERN.matcher(text);
    String line;
    String subText;
    while (match.find()) {
      if (type == 1) {
        subText = "(" + XMLManager.getText("Script_Command_Enter") + " " +
                  XMLManager.getText("Script_Command_Enter_Left") + ": ";
      } else {
        subText = "(" + XMLManager.getText("Script_Command_Enter") + " " +
                  XMLManager.getText("Script_Command_Enter_Top") + ": ";
      }
      switch (match.group(2)) {
        case "0":
        case "2":
        case "3":
          break;
        case "6":
        case "7":
          if (type == 1) {
            subText = "(" + XMLManager.getText("Script_Command_Enter") + " " +
                      XMLManager.getText("Script_Command_Enter_Right") + ": ";
          } else {
            subText = "(" + XMLManager.getText("Script_Command_Enter") + " " +
                      XMLManager.getText("Script_Command_Enter_Bottom") + ": ";
          }
          break;
      }
      // If the enter pattern has modifiers, we need to check those.
      if (match.group(3) != null) {
        int i = text.indexOf(match.group(0)) + match.group(0).length();
        // If the enter pattern is followed immediately by anything related to
        // displaying a line of text, we leave it there so that it can be parsed
        // as a speaking pattern.
        line = text.substring(i, Math.min(i + 20, text.length()));
        if (!SKIP_MODIFIERS.matcher(line).find()) {
          String emotion = processEmotion(match.group(3));
          if (line.startsWith("$Wm") || line.startsWith("$Ws")) {
            text = text.replaceFirst(Pattern.quote(match.group(0)),
                Matcher.quoteReplacement(String.format(ENTER_FORMAT[3], subText,
                 CharacterManager.getTranslatedName(match.group(1)), emotion)));
          } else {
            text = text.replaceFirst(Pattern.quote(match.group(0)),
                Matcher.quoteReplacement(String.format(ENTER_FORMAT[0], subText,
                    CharacterManager.getTranslatedName(match.group(1)), emotion,
                             "$Ws" + match.group(1) + "|")));
          }
        // If it isn't, we process it all as an enter pattern.
        } else {
          text = text.replaceFirst(Pattern.quote(match.group(0)),
                Matcher.quoteReplacement(String.format(ENTER_FORMAT[1], subText,
             CharacterManager.getTranslatedName(match.group(1)), match.group(1),
                                                              match.group(3))));
        }
      // If it doesn't, we can just use the simple enter pattern.
      } else {
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                Matcher.quoteReplacement(String.format(ENTER_FORMAT[2], subText,
                          CharacterManager.getTranslatedName(match.group(1)))));
      }
    }
  }
  
  /**
   * Replaces exit patterns with script commands.
   */
  private void replaceExitPatterns() {
    match = EXIT_PATTERN.matcher(text);
    while (match.find()) {
      // We need to make sure the exit isn't meant to apply to the last used
      // character by inference.
      if (match.group(1) != null) {
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                             Matcher.quoteReplacement(String.format(EXIT_FORMAT,
                          CharacterManager.getTranslatedName(match.group(1)))));
      // If it is, we can just use the simple exit pattern.
      } else {
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                                 Matcher.quoteReplacement("(" +
                            XMLManager.getText("Script_Command_Exit") + ")\n"));
      }
    }
  }
  
  /**
   * Replaces the Has Permanents pattern with a script command.
   */
  private void replaceHasPermanentsPattern() {
    if ((match = PERMANENT_PATTERN.matcher(text)).find()) {
      text = text.replaceFirst(Pattern.quote(match.group(0)),
                               Matcher.quoteReplacement("(" +
                   XMLManager.getText("Script_Command_Has_Permanents") +")\n"));
    }
  }
  
  /**
   * Replaces sound effect patterns with script commands.
   */
  private void replaceSoundEffectPatterns() {
    match = SOUND_EFFECT_PATTERN.matcher(text);
    while (match.find()) {
      text = text.replaceFirst(Pattern.quote(match.group(0)),
                     Matcher.quoteReplacement(String.format(SOUND_EFFECT_FORMAT,
                                                            match.group(1))));
    }
  }
  
  /**
   * Replaces speaking patterns with script commands.
   */
  private void replaceSpeakingPatterns() {
    match = SPEAKING_PATTERN.matcher(text);
    int i;
    int i2;
    int i3;
    String[] ex;
    StringBuilder exText;
    StringBuilder beforeEmotion;
    StringBuilder beforeSoundEffect;
    StringBuilder first;
    StringBuilder follow;
    StringBuilder inLine;
    StringBuilder inLineAfter;
    StringBuilder last;
    String[] modifiers;
    String[] rgba;
    String line;
    String soundEffect;
    String emotion;
    String subText;
    boolean hadColor;
    while (match.find()) {
      hadColor = false;
      emotion = "";
      soundEffect = "";
      first = new StringBuilder();
      beforeSoundEffect = new StringBuilder(" {");
      beforeEmotion = new StringBuilder(" {");
      exText = new StringBuilder();
      follow = new StringBuilder();
      last = new StringBuilder();
      // If there are any commands, we need to process them.
      if (match.group(2) != null) {
        modifiers = match.group(2).split("\\|");
        for (String s : modifiers) {
          if (s.startsWith("$Wa")) {
            s = s.substring(3);
          }
          if (s.startsWith("$E")) {
            emotion = " (" + processEmotion(s.substring(2)) + ")";
          } else if (s.startsWith("$Svp")) {
            soundEffect = " [" + s.substring(4) + "]";
          } else if (s.startsWith("$Wv$Svp")) {
            beforeSoundEffect.append("$Wv");
            soundEffect = " [" + s.substring(7) + "]";
          } else if (s.startsWith("$Ws")) {
            if (!s.substring(3).equals(match.group(1))) {
              follow.append(s).append("|");
            }
          } else if (s.startsWith("$c")) {
            rgba = s.substring(2).split(",");
            if (hadColor) {
              last.append("(");
              last.append(XMLManager.getText("Script_Command_Text_Color"));
              last.append(": ");
              last.append(rgba[0]).append(", ");
              last.append(rgba[1]).append(", ");
              last.append(rgba[2]).append(", ");
              last.append(rgba[3]).append(")\n");
            } else {
              last.append("(");
              last.append(XMLManager.getText("Script_Command_Text_Color"));
              last.append(": ");
              first.append(rgba[0]).append(", ");
              first.append(rgba[1]).append(", ");
              first.append(rgba[2]).append(", ");
              first.append(rgba[3]).append(")\n");
              hadColor = true;
            }
          } else if (s.startsWith("$")) {
            if (soundEffect.isEmpty()) {
              beforeSoundEffect.append(s).append("|");
            } else if (emotion.isEmpty()) {
              beforeEmotion.append(s).append("|");
            } else {
              follow.append(s).append("|");
            }
          } else {
            follow.append(s);
          }
        }
        // Now we need to see if the next line is a followup from the same
        // character.
        if (match.group(3) != null) {
          i3 = text.indexOf(match.group(0)) + match.group(0).length();
          subText = text.substring(i3, Math.min(i3 + 30, text.length()));
          if (!subText.startsWith("\n$Ws")) {
            ex = match.group(3).split("\n\\$k\\$p");
            exText.append(ex[0]);
            i = 0;
            i2 = 1;
            while ((i = match.group(3).indexOf("\n$k$p", i) + 1) > 0) {
              i2++;
            }
            for (i = 1; i < ex.length; i++) {
              line = ex[i];
              if (line.startsWith("$W") || line.startsWith("(E") ||
                  line.startsWith("$k$p(E") || line.startsWith("$Svp")) {
                for (i3 = i; i3 < ex.length; i3++) {
                  if (ex[i3].startsWith("$W") || ex[i3].startsWith("$Svp")) {
                    exText.append("\n$k$p").append(ex[i3]);
                  } else {
                    exText.append("\n").append(ex[i3]);
                  }
                }
                break;
              }
              exText.append("\n");
              exText.append(CharacterManager.getTranslatedName(match.group(1)));
              modifiers = line.split("\\|");
              inLine = new StringBuilder();
              inLineAfter = new StringBuilder();
              for (String s : modifiers) {
                if (s.startsWith("$E")) {
                  inLine.append(" (").append(processEmotion(s.substring(2)));
                  inLine.append(")");
                } else if (s.startsWith("$Svp")) {
                  inLine.insert(0, " [");
                  inLine.insert(2, s.substring(4));
                  inLine.insert(2 + s.length() - 4, "]");
                } else {
                  if (s.startsWith("$") || s.matches("^\\d+$")) {
                    inLineAfter.append(s).append("|");
                  } else {
                    inLineAfter.append(s);
                  }
                }
              }
              exText.append(inLine).append(": ").append(inLineAfter);
            }
            if (i2 > ex.length) {
              if (subText.startsWith("(E")) {
                exText.append("\n");
              } else {
                exText.append("\n");
                exText.append(CharacterManager.getTranslatedName(
                                                               match.group(1)));
                exText.append(": ");
              }
            }
          } else {
            exText.append(match.group(3));
          }
        }
      }
      if (beforeSoundEffect.toString().length() > 2) {
        beforeSoundEffect.append("}");
      } else {
        beforeSoundEffect.delete(0, 2);
      }
      if (beforeEmotion.toString().length() > 2) {
        beforeEmotion.append("}");
      } else {
        beforeEmotion.delete(0, 2);
      }
      subText = String.format(SPEAKING_FORMAT, first.toString(),
                             CharacterManager.getTranslatedName(match.group(1)),
                              beforeSoundEffect.toString(), soundEffect,
                              beforeEmotion.toString(), emotion,
                              follow.toString(), exText.toString(),
                              last.toString());
      text = text.replaceFirst(Pattern.quote(match.group(0)),
                                             Matcher.quoteReplacement(subText));
      match = SPEAKING_PATTERN.matcher(text);
    }
  }
  
  /**
   * Replaces type patterns with script commands.
   */
  private void replaceTypePattern() {
    if ((match = TYPE_PATTERN.matcher(text)).find()) {
      if (match.group(1).equals("0")) {
        type = 0;
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                                 Matcher.quoteReplacement("(" +
                              XMLManager.getText("Script_Command_Type") + ": " +
                       XMLManager.getText("Script_Command_Type_Zero") + ")\n"));
      } else {
        type = 1;
        text = text.replaceFirst(Pattern.quote(match.group(0)),
                                 Matcher.quoteReplacement("(" +
                              XMLManager.getText("Script_Command_Type") + ": " +
                        XMLManager.getText("Script_Command_Type_One") + ")\n"));
      }
    }
  }
  
  /**
   * Restores gender substitution patterns.
   */
  private void restoreGenderSubstitutions() {
    match = INSG_PATTERN.matcher(text);
    int i = 0;
    while (match.find()) {
      text = text.replaceFirst(match.group(0),
                               Matcher.quoteReplacement(gValues.get(i)));
      i++;
    }
  }
  
  /**
   * Runs the game text converter.
   * 
   * @return the game text converted into a script
   */
  public String run() {
    if (!finished) {
      // We cut out and store every instance of the $G pattern before doing any
      // other kind of processing to avoid the risk of it causing problems. It
      // cannot itself hold patterns, so this is not an issue.
      extractGenderSubstitutions();
      text = INVALID_CHARACTER_REFERENCE.matcher(text).replaceAll("\\$Wm");
      text = text.replaceAll("\\$k", "\n\\$k");
      replaceEnterPatterns();
      // Now we want to split the text by each instance of a character command.
      text = text.replaceAll("\\|\\$Ws", "|\n\\$Ws");
      replaceExitPatterns();
      replaceHasPermanentsPattern();
      replaceTypePattern();
      replaceBGMPatterns();
      replaceSpeakingPatterns();
      replaceEmotionPatterns();
      replaceSoundEffectPatterns();
      for (SubstitutionPair pair : FINAL_SUBSTITUTIONS) {
        text = pair.replaceAll(text);
      }
      text = "[" + XMLManager.getText("Script_Command_Formatted_Script") +
             "]\n" + text;
      restoreGenderSubstitutions();
      text = text.replaceFirst("\n\\$k$", "");
      finished = true;
    }
    return text;
  }
  
  /**
   * Sets the converter's text.
   * 
   * @param value the converter's text
   */
  public void setText(String value) {
    text = value;
  }
}
