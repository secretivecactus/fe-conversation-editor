/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.text;

import java.util.regex.Pattern;

/**
  * Provides a small wrapper for a regular expression substitution with a fixed
  * replacement.
  * 
  * @author Secretive Cactus
  */
public class SubstitutionPair {
   
  /**
   * The pattern that the substitution pair will replace.
   */
  private final Pattern pattern;
  
  /**
   * The text with which the substitution pair will replace its pattern.
   */
  private final String replacement;
  
  /**
   * Creates a new substitution pair for replacing the given pattern with its
   * given replacement.
   * 
   * @param pattern the pattern to replace
   * @param replacement the text with which to replace it
   */
  public SubstitutionPair(String pattern, String replacement) {
    this.pattern = Pattern.compile(pattern);
    this.replacement = replacement;
  }
  
  /**
   * Replaces every instance of the substitution pair's pattern in the passed
   * text.
   * 
   * @param text the text in which the pattern should be replaced
   * @return the text with all of the occurrences of the pattern replaced
   */
  public String replaceAll(String text) {
    return pattern.matcher(text).replaceAll(replacement);
  }
  
  /**
   * Replaces the first instance of the substitution pair's pattern in the
   * passed text.
   * 
   * @param text the text in which the pattern should be replaced
   * @return the text with the first occurrence of the pattern replaced
   */
  public String replaceFirst(String text) {
    return pattern.matcher(text).replaceFirst(replacement);
  }
}