/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains general utility methods for working with byte data.
 * 
 * @author Secretive Cactus
 */
public final class ByteUtils {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                                     ByteUtils.class.getName());
  
  /**
   * Converts an integer into its byte representation.
   * 
   * @param i the integer that should be converted to bytes
   * @return the byte representation of the integer
   */
  public static byte[] getBytes(int i) {
    byte[] result = new byte[4];
    if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
      result[0] = (byte) (i);
      result[1] = (byte) (i >> 8);
      result[2] = (byte) (i >> 16);
      result[3] = (byte) (i >> 24);
    } else {
      result[0] = (byte) (i >> 24);
      result[1] = (byte) (i >> 16);
      result[2] = (byte) (i >> 8);
      result[3] = (byte) (i);
    }
    return result;
  }
  
  /**
   * Reads a file's bytes into an array and returns them.
   * 
   * @param source the file from which the bytes should be read
   * @return the file's bytes
   */
  public static byte[] getFileBytes(File source) {
    byte[] bytes;
    try (FileInputStream input = new FileInputStream(source)) {
      bytes = new byte[(int) source.length()];
      input.read(bytes);
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
      bytes = new byte[0];
    }
    return bytes;
  }
  
  /**
   * Reads an unsigned 16-bit integer from the specified index in a byte array.
   * 
   * @param bytes the array of bytes from which the integer will be read
   * @param index the index at which the integer is found
   * @return the 16-bit unsigned integer at the specified index
   */
  public static int getUInt16(byte[] bytes, int index) {
    if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
      return ((bytes[index + 1] & 0xff) << 8) + (bytes[index] & 0xff);
    } else {
      return ((bytes[index] & 0xff) << 8) + (bytes[index + 1] & 0xff);
    }
  }
  
  /**
   * Reads an unsigned 24-bit integer from the specified index in a byte array.
   * 
   * @param bytes the array of bytes from which the integer will be read
   * @param index the index at which the integer is found
   * @return the 24-bit unsigned integer at the specified index
   */
  public static int getUInt24(byte[] bytes, int index) {
    if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
      return ((bytes[index + 2] & 0xff) << 16) +
             ((bytes[index + 1] & 0xff) << 8) + (bytes[index] & 0xff);
    } else {
      return ((bytes[index + 0] & 0xff) << 16) +
             ((bytes[index + 1] & 0xff) << 8) + (bytes[index + 2] & 0xff);
    }
  }
  
  /**
   * Reads an unsigned 32-bit integer from the specified index in a byte array.
   * 
   * @param bytes the array of bytes from which the integer will be read
   * @param index the index at which the integer is found
   * @return the 32-bit unsigned integer at the specified index
   */
  public static int getUInt32(byte[] bytes, int index) {
    if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
      return ((bytes[index + 3] & 0xff) << 24) +
             ((bytes[index + 2] & 0xff) << 16) +
             ((bytes[index + 1] & 0xff) << 8) + (bytes[index] & 0xff);
    } else {
      return ((bytes[index] & 0xff) << 24) +
             ((bytes[index + 1] & 0xff) << 16) +
             ((bytes[index + 2] & 0xff) << 8) + (bytes[index + 3] & 0xff);
    }
  }
}
