/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: ofs.you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at ofs.your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You ofs.should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.wrapper;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import jfxtras.labs.util.ColorUtils;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.util.io.ByteUtils;
import sahara.cactus.fireemblem.conversation.util.control.CanvasUtils;

/**
 * Contains character data that can be drawn onto a Canvas.
 * <p>
 * The drawing methods were drawn from SciresM's FEAT.
 * 
 * @author SciresM
 * @author Secretive Cactus
 */
public class CharacterBust {
  
  /**
   * Contains offsets for drawing an image.
   */
  protected class Offsets {
    
    /**
     * The image's x position.
     */
    protected int x;
    
    /**
     * The image's y position.
     */
    protected int y;
    
    /**
     * The image's source x.
     */
    protected int sx;
    
    /**
     * The image's source y.
     */
    protected int sy;
    
    /**
     * The image's source width.
     */
    protected int sw;
    
    /**
     * The image's source height.
     */
    protected int sh;
    
    /**
     * The image's emotion x position.
     */
    protected double emoX;
    
    /**
     * The image's emotion y position.
     */
    protected double emoY;
  }
  
  /**
   * The default emotion for a character.
   */
  private static final String DEFAULT_EMOTION = "通常,";
  
  /**
   * The character's name.
   */
  protected StringProperty name;
  
  /**
   * The character's emotion.
   */
  protected String emotion;
  
  /**
   * The character's hair color.
   */
  protected ObjectProperty<Color> hairColor;
  
  /**
   * Whether or not the character should be drawn mirrored.
   */
  protected boolean flip;

  /**
   * Creates an empty character bust.
   */
  public CharacterBust() {
    name = new SimpleStringProperty("");
    emotion = DEFAULT_EMOTION;
    hairColor = new SimpleObjectProperty<>(Color.rgb(0x5B, 0x58, 0x55));
    flip = false;
  }
  
  /**
   * Creates a character bust that begins with the values for the specified
   * character.
   * 
   * @param name the name of the character represented by the bust
   */
  public CharacterBust(String name) {
    this.name = new SimpleStringProperty(name);
    emotion = DEFAULT_EMOTION;
    hairColor = new SimpleObjectProperty<>(Color.rgb(0x5B, 0x58, 0x55));
    this.flip = false;
  }
  
  /**
   * Creates a character bust that begins with the values for the specified
   * character and emotion.
   * 
   * @param name the name of the character represented by the bust
   * @param emotion the name of the bust's initial emotion
   */
  public CharacterBust(String name, String emotion) {
    this.name = new SimpleStringProperty(name);
    this.emotion = emotion;
    hairColor = new SimpleObjectProperty<>(Color.rgb(0x5B, 0x58, 0x55));
    this.flip = false;
  }
  
  /**
   * Creates a character bust that begins with the values for the specified
   * character, emotion, and hair color.
   * 
   * @param name the name of the character represented by the bust
   * @param emotion the name of the bust's initial emotion
   * @param color the color of the character's hair
   */
  public CharacterBust(String name, String emotion, Color color) {
    this.name = new SimpleStringProperty(name);
    this.emotion = emotion;
    this.hairColor = new SimpleObjectProperty<>(color);
    this.flip = false;
  }

  /**
   * Creates a character bust that begins with the values for the specified
   * character, emotion, hair color, and mirrored status.
   * 
   * @param name the name of the character represented by the bust
   * @param emotion the name of the bust's initial emotion
   * @param color the color of the character's hair
   * @param flip whether or not the bust should be drawn mirrored
   */
  public CharacterBust(String name, String emotion, Color color, boolean flip) {
    this.name = new SimpleStringProperty(name);
    this.emotion = emotion;
    this.hairColor = new SimpleObjectProperty<>(color);
    this.flip = true;
  }
  
  /**
   * Creates an empty character bust with the specified mirrored status.
   * 
   * @param flip whether or not the bust should be drawn mirrored
   */
  public CharacterBust(boolean flip) {
    name = new SimpleStringProperty("");
    emotion = DEFAULT_EMOTION;
    hairColor = new SimpleObjectProperty<>(Color.rgb(0x5B, 0x58, 0x55));
    this.flip = flip;
  }

  /**
   * Resets the bust to an empty state.
   */
  public void clear() {
    name.set("");
    emotion = DEFAULT_EMOTION;
    hairColor = new SimpleObjectProperty<>(Color.rgb(0x5B, 0x58, 0x55));
  }
  
  /**
   * Draws the character's bust on the specified canvas.
   * 
   * @param canvas the canvas on which the bust should be drawn
   * @param drawY the y at which the bust should be drawn
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void drawCharacterBustImage(Canvas canvas, int drawY) throws
                                                    GameBranchMissingException {
    if (name.get().startsWith("username")) {
      CharacterManager.getMyUnit().flip = flip;
      CharacterManager.getMyUnit().emotion = emotion;
      CharacterManager.getMyUnit().drawCharacterBustImage(canvas, drawY);
      return;
    } else if (name.get().isEmpty()) {
      return;
    }
    String name = this.name.get();
    if (name.equals("ベロア")) {
      name = "べロア";
    }
    String hairName = name + "_bu_髪" + "0.png";
    String datId = "FSID_BU_" + name;
    String[] emotions = emotion.split(",");
    String resName = name + "_bu_" + emotions[0] + ".png";
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      CanvasUtils.mirrorCanvas(canvas);
      GraphicsContext g = canvas.getGraphicsContext2D();
      g.setGlobalBlendMode(BlendMode.SRC_OVER);
      Image res = ResourceManager.loadImage("face/" + resName);
      Offsets ofs = new Offsets();
      GameMode.branch(() -> { // Awakening
        ofs.sx = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x10);
        ofs.sy = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x12);
        ofs.sw = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x14);
        ofs.sh = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x16);
      },
      () -> { // Fates
        ofs.sx = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x30);
        ofs.sy = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x32);
        ofs.sw = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x34);
        ofs.sh = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x36);
      });
      ofs.x = -12 - ofs.sw;
      if (flip) {
        GameMode.branch(() -> { //Awakening
          ofs.y = 3;
        }, () -> { // Fates
          ofs.y = 6;
        });
      } else {
        GameMode.branch(() -> { //Awakening
          ofs.y = drawY - 17;
        }, () -> { // Fates
          ofs.y = drawY + 3;
        });
      }
      g.drawImage(res, ofs.sx, ofs.sy, ofs.sw, ofs.sh, ofs.x, ofs.y, ofs.sw,
                  ofs.sh);
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_bu_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            GameMode.branch(() -> { // Awakening
              ofs.emoX = ofs.x - ofs.sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x20);
              ofs.emoY = ofs.y - ofs.sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x22);
            }, () -> { // Fates
              ofs.emoX = ofs.x - ofs.sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x40);
              ofs.emoY = ofs.y - ofs.sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x42);
            });
            g.drawImage(ResourceManager.loadImage("face/" + exResName),
                        ofs.emoX, ofs.emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            GameMode.branch(() -> { // Awakening
              ofs.emoX = ofs.x - ofs.sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x18);
              ofs.emoY = ofs.y - ofs.sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x1A);
            }, () -> { // Fates
              ofs.emoX = ofs.x - ofs.sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x38);
              ofs.emoY = ofs.y - ofs.sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x3A);
            });
            g.drawImage(ResourceManager.loadImage("face/" + exResName),
                        ofs.emoX, ofs.emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(res, ofs.sx, ofs.sy, ofs.sw, ofs.sh, ofs.x, ofs.y, ofs.sw,
                    ofs.sh);
      }
    }
  }
  
  /**
   * Draws the character's full torso image on the specified canvas.
   * 
   * @param canvas the canvas on which the image should be drawn
   * @param isActive whether or not the character is the active speaker
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void drawCharacterStageImage(Canvas canvas, boolean isActive) throws
                                                    GameBranchMissingException {
    if (name.get().startsWith("username")) {
      CharacterManager.getMyUnit().flip = flip;
      CharacterManager.getMyUnit().emotion = emotion;
      CharacterManager.getMyUnit().drawCharacterStageImage(canvas,
                                                                 isActive);
      return;
    }
    String name = this.name.get();
    if (name.equals("ベロア")) {
      name = "べロア";
    }
    String hairName = name + "_st_髪" + "0.png";
    String datId = "FSID_ST_" + name;
    String[] emotions = emotion.split(",");
    String resName = name + "_st_" + emotions[0] + ".png";
    CanvasUtils.normalizeCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      Offsets ofs = new Offsets();
      Image res = ResourceManager.loadImage("face/" + resName);
      if (flip) {
        CanvasUtils.mirrorCanvas(canvas);
        ofs.x = (int) (28 - res.getWidth());
        ofs.y = (int) (canvas.getHeight() - res.getHeight() + 14);
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), ofs.x,
                    ofs.y);
      } else {
        ofs.x = (int) (canvas.getWidth() - res.getWidth() + 28);
        ofs.y = (int) (canvas.getHeight() - res.getHeight() + 14);
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), ofs.x,
                    ofs.y);
      }
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_st_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            GameMode.branch(() -> { // Awakening
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x20);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x22);
            }, () -> { // Fates
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x40);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x42);
            });
            g.drawImage(ResourceManager.loadImage("face/" + exResName),
                        ofs.emoX, ofs.emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            GameMode.branch(() -> { // Awakening
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x18);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x1A);
            }, () -> { // Fates
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x38);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x3A);
            });
            g.drawImage(ResourceManager.loadImage("face/" + exResName),
                        ofs.emoX, ofs.emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), ofs.x,
                    ofs.y);
      }
    }
  }
  
  /**
   * Draws the character's full torso image on the specified canvas.
   * 
   * @param canvas the canvas on which the image should be drawn
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void drawCharacterStageImage(Canvas canvas) throws
                                                    GameBranchMissingException {
    if (name.get().startsWith("username")) {
      CharacterManager.getMyUnit().flip = flip;
      CharacterManager.getMyUnit().emotion = emotion;
      CharacterManager.getMyUnit().drawCharacterStageImage(canvas);
      return;
    }
    String name = this.name.get();
    if (name.equals("ベロア")) {
      name = "べロア";
    }
    String hairName = name + "_st_髪" + "0.png";
    String datId = "FSID_ST_" + name;
    String[] emotions = emotion.split(",");
    String resName = name + "_st_" + emotions[0] + ".png";
    CanvasUtils.normalizeCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      Offsets ofs = new Offsets();
      Image res = ResourceManager.loadImage("face/" + resName);
      if (flip) {
        CanvasUtils.mirrorCanvas(canvas);
      }
      g.drawImage(res, 0, 0);
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_st_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            GameMode.branch(() -> { // Awakening
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x20);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x22);
            }, () -> { // Fates
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x40);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x42);
            });
            g.drawImage(ResourceManager.loadImage("face/" + exResName),
                        ofs.emoX, ofs.emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            GameMode.branch(() -> { // Awakening
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x18);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x1A);
            }, () -> { // Fates
              ofs.emoX = ofs.x + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x38);
              ofs.emoY = ofs.y + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x3A);
            });
            g.drawImage(ResourceManager.loadImage("face/" + exResName),
                        ofs.emoX, ofs.emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(res, 0, 0);
      }
    }
  }
  
  /**
   * Returns whether or not the bust is equal to another bust.
   * 
   * @param other the bust to test for equality
   * @return whether or not the two busts are equal
   */
  public boolean equals(CharacterBust other) {
    return other.name.get().equals(name.get()) && other.flip == flip &&
           other.hairColor.get().equals(hairColor.get());
  }
  
  /**
   * Gets the bust's current emotion.
   * 
   * @return the bust's current emotion
   */
  public String getEmotion() {
    return emotion;
  }
  
  /**
   * Gets whether or not the bust should be mirrored.
   * 
   * @return whether or not the bust should be mirrored
   */
  public boolean getFlip() {
    return flip;
  }
  
  /**
   * Gets the bust's hair color.
   * 
   * @return the bust's hair color
   */
  public Color getHairColor() {
    return hairColor.get();
  }
  
  /**
   * Returns the bust's character name.
   * 
   * @return the bust's character name
   * @see #toString()
   */
  public String getName() {
    return name.get();
  }
  
  /**
   * The property containing the bust's hair color.
   * 
   * @return the property containing the bust's hair color
   */
  public ObjectProperty<Color> hairColorProperty() {
    return hairColor;
  }
  
  /**
   * The property containing the bust's name.
   * 
   * @return the property containing the bust's name
   */
  public StringProperty nameProperty() {
    return name;
  }
  
  /**
   * Resets the bust to its default emotion.
   */
  public void resetEmotion() {
    emotion = DEFAULT_EMOTION;
  }

  /**
   * Resets the bust to its default emotion and changes its associated
   * character.
   * 
   * @param character the character that should be associated with the bust
   */
  public void resetEmotion(String character) {
    name.set(character);
    emotion = DEFAULT_EMOTION;
  }
  
  /**
   * Sets the bust's values to be the same as those of another bust.
   * 
   * @param other the bust whose values should be applied to this bust
   */
  public void set(CharacterBust other) {
    name.set(other.name.get());
    emotion = other.emotion;
    hairColor.set(other.hairColor.get());
    flip = other.flip;
  }
  
  /**
   * Sets the bust's emotion.
   * 
   * @param value the bust's emotion
   */
  public void setEmotion(String value) {
    emotion = value;
  }
  
  /**
   * Sets whether or not the bust should be mirrored.
   * 
   * @param value whether or not the bust should be mirrored
   */
  public void setFlip(boolean value) {
    flip = value;
  }
  
  /**
   * Sets the bust's hair color.
   * 
   * @param value the bust's hair color
   */
  public void setHairColor(Color value) {
    hairColor.set(value);
  }
  
  /**
   * Sets the character associated with the bust.
   * 
   * @param value the character to associate with the bust
   */
  public void setName(String value) {
    name.set(value);
    String hC = CharacterManager.getMyUnit().getChildHairColors().get(value);
    if (hC != null) {
      hairColor.set(ColorUtils.webColorToColor(hC));
    } else {
      CharacterReference ref = CharacterManager.getByName(CharacterManager
                                                     .getTranslatedName(value));
      if (ref != null) {
        hairColor.set(ref.getHairColor());
      }
    }
  }
  
  /**
   * Returns the bust's character name.
   * 
   * @return the bust's character name
   * @see #getName()
   */
  @Override
  public String toString() {
    return getName();
  }
}
