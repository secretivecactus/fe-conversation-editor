/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.myunit;

import java.util.Set;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import jfxtras.labs.util.ColorUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.util.io.ByteUtils;
import sahara.cactus.fireemblem.conversation.util.control.CanvasUtils;

/**
 * Contains Kamui data that can be drawn onto a Canvas.
 * <p>
 * The drawing methods were drawn from SciresM's FEAT.
 * 
 * @author SciresM
 * @author Secretive Cactus
 */
public class Kamui extends MyUnit {
  
  /**
   * The list of eye styles available to a Kamui.
   */
  private static final String[] EYE_STYLES = {
                                               "a", "b", "c", "d", "e", "f", "g"
                                             };
  
  /**
   * The list of bodies available to a Kamui.
   */
  private static final String[][] BODIES = {
                                             { "マイユニ女1", "マイユニ女2" },
                                             { "マイユニ男1", "マイユニ男2" }
                                           };
  
  /**
   * The list of datId names available to a Kamui.
   */
  private static final String[][] DAT_NAMES = {
                                                { "マイユニ_女1", "マイユニ_女2" },
                                                { "マイユニ_男1", "マイユニ_男2" }
                                              };
  
  /**
   * The index of the Kamui's first accessory.
   */
  private int accessory1;
  
  /**
   * The index of the Kamui's second accessory.
   */
  private int accessory2;
  
  /**
   * Creates a Kamui with default name for the current game mode.
   */
  public Kamui() {
    super();
    body = 1;
    accessory1 = 0;
    accessory2 = 0;
  }
  
  /**
   * Creates a Kamui with the given name.
   * 
   * @param name the Kamui's name
   */
  public Kamui(String name) {
    super(name);
    accessory1 = 0;
    accessory2 = 0;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void drawCharacterBustImage(Canvas canvas, int drawY) {
    String hairName = getHairName("bu");
    String datId = getDatId("BU");
    String name = EYE_STYLES[eyes] + BODIES[gender.ordinal()][body];
    String[] emotions = emotion.split(",");
    String resName = name + "_bu_" + emotions[0] + ".png";
    CanvasUtils.mirrorCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      int x;
      int y;
      int sx = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x30);
      int sy = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x32);
      int sw = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x34);
      int sh = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x36);
      Image res = ResourceManager.loadImage("face/" + resName);
      x = -12 - sw;
      if (flip) {
        y = 6;
      } else {
        // REVISIT
        y = 114 + sh;
      }
      g.drawImage(res, sx, sy, sw, sh, x, y, sw, sh);
      if (accessory1 > 0) {
        res = ResourceManager.loadImage(getAccessory1Name("bu"));
        g.drawImage(res, x - sx, y - sy);
      }
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        int emoX;
        int emoY;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_bu_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x - sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x40);
            emoY = y - sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x42);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x - sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x38);
            emoY = y - sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x3A);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(res, sx, sy, sw, sh, x, y, sw, sh);
      }
      if (accessory2 > 0) {
        res = ResourceManager.loadImage(getAccessory2Name("bu"));
        if (body == 0 || body == 2) {
          g.drawImage(res, 0, sy - 5, (sw - 60) + sx, sh + 5, x - sx + 60,
                    y, (sw - 60) + sx, sh);
        } else {
          g.drawImage(res, 0, sy - 21, (sw - 59) + sx, sh + 5, x - sx + 59,
                    y, (sw - 59) + sx, sh);
        }
      }
    }
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void drawCharacterStageImage(Canvas canvas, boolean isActive) {
    String hairName = getHairName("st");
    String datId = getDatId("ST");
    String name = EYE_STYLES[eyes] + BODIES[gender.ordinal()][body];
    String[] emotions = emotion.split(",");
    String resName = name + "_st_" + emotions[0] + ".png";
    CanvasUtils.normalizeCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      int x = 0;
      int y = 0;
      Image res = ResourceManager.loadImage("face/" + resName);
      if (flip) {
        CanvasUtils.mirrorCanvas(canvas);
        x = (int) (28 - res.getWidth());
        y = (int) (canvas.getHeight() - res.getHeight() + 14);
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      } else {
        x = (int) (canvas.getWidth() - res.getWidth() + 28);
        y = (int) (canvas.getHeight() - res.getHeight() + 14);
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      }
      if (accessory1 > 0) {
        res = ResourceManager.loadImage(getAccessory1Name("st"));
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      }
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        int emoX;
        int emoY;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_st_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x40);
            emoY = y + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x42);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x38);
            emoY = y + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x3A);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      }
      if (accessory2 > 0) {
        res = ResourceManager.loadImage(getAccessory2Name("st"));
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x + 124,
                    y + 27);
      }
    }
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void drawCharacterStageImage(Canvas canvas) {
    String hairName = getHairName("st");
    String datId = getDatId("ST");
    String name = EYE_STYLES[eyes] + BODIES[gender.ordinal()][body];
    String[] emotions = emotion.split(",");
    String resName = name + "_st_" + emotions[0] + ".png";
    CanvasUtils.normalizeCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      Image res = ResourceManager.loadImage("face/" + resName);
      if (flip) {
        CanvasUtils.mirrorCanvas(canvas);
      }
      g.drawImage(res, 0, 0);
      if (accessory1 > 0) {
        res = ResourceManager.loadImage(getAccessory1Name("st"));
        g.drawImage(res, 0, 0);
      }
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        int emoX;
        int emoY;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_st_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x40);
            emoY = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x42);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x38);
            emoY = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x3A);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(res, 0, 0);
      }
      if (accessory2 > 0) {
        res = ResourceManager.loadImage(getAccessory2Name("st"));
        g.drawImage(res, 124, 27);
      }
    }
  }
  
  /**
   * Converts the passed My Unit XML nodes to a Kamui.
   * 
   * @param nodes the nodes containing the Kamui's data
   * @return a Kamui with data drawn from the passed nodes
   */
  public static Kamui fromXML(NodeList nodes) {
    Kamui myUnit = new Kamui();
    int len = nodes.getLength();
    Node node;
    String name;
    for (int i = 0; i < len; i++) {
      node = nodes.item(i);
      switch (node.getNodeName()) {
        case "name":
          myUnit.name.set(node.getTextContent());
          break;
        case "voice":
          myUnit.voice = node.getTextContent();
          break;
        case "gender":
          myUnit.gender = Gender.valueOf(node.getTextContent().toLowerCase());
          break;
        case "body":
          myUnit.body = Integer.parseInt(node.getTextContent());
          break;
        case "hair_style":
          myUnit.hairStyle = Integer.parseInt(node.getTextContent());
          break;
        case "hair_color":
          myUnit.hairColor.set(ColorUtils.webColorToColor(
                                                        node.getTextContent()));
          break;
        case "eyes":
          myUnit.eyes = Integer.parseInt(node.getTextContent());
          break;
        case "accessory1":
          myUnit.accessory1 = Integer.parseInt(node.getTextContent());
          break;
        case "accessory2":
          myUnit.accessory2 = Integer.parseInt(node.getTextContent());
          break;
        case "child_hair_color":
          name = node.getAttributes().getNamedItem("target").getNodeValue();
          myUnit.childHairColors.put(name, node.getTextContent());
          break;
      }
    }
    return myUnit;
  }
  
  /**
   * Gets the My Unit's accessory 1 index.
   * 
   * @return the My Unit's accessory 1 index
   */
  public int getAccessory1() {
    return accessory1;
  }
  
  /**
   * Gets the name of the My Unit's accessory 1 image.
   * 
   * @param type "st" for a stage image or "bu" for a bust image
   * @return the name of the My Unit's accessory 1 image
   */
  public String getAccessory1Name(String type) {
    return "face/" + BODIES[gender.ordinal()][body] + "_" + type +
           "_アクセサリ1_" + accessory1 + ".png";
  }
  
  /**
   * Gets the My Unit's accessory 2 index.
   * 
   * @return the My Unit's accessory 2 index
   */
  public int getAccessory2() {
    return accessory2;
  }
  
  /**
   * Gets the name of the My Unit's accessory 2 image.
   * 
   * @param type "st" for a stage image or "bu" for a bust image
   * @return the name of the My Unit's accessory 2 image
   */
  public String getAccessory2Name(String type) {
    return "face/" + BODIES[gender.ordinal()][body] + "_" + type +
           "_アクセサリ2_" + accessory2 + ".png";
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getCharacterImageName() {
    return EYE_STYLES[eyes] + BODIES[gender.ordinal()][body];
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getDatId(String type) {
    return "FSID_" + type + "_" + DAT_NAMES[gender.ordinal()][body] + "_顔" +
           EYE_STYLES[eyes].toUpperCase();
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getHairName(String type) {
    return BODIES[gender.ordinal()][body] + "_" + type + "_髪" + hairStyle +
           ".png";
  }
  
  /**
   * Sets the My Unit's accessory 1 index.
   * 
   * @param value the My Unit's accessory 1 index
   */
  public void setAccessory1(int value) {
    accessory1 = value;
  }
  
  /**
   * Sets the My Unit's accessory 2 index.
   * 
   * @param value the My Unit's accessory 2 index
   */
  public void setAccessory2(int value) {
    accessory2 = value;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public Node toXML(Document document) {
    Element kamui = document.createElement("myunit");
    Element attribute;
    attribute = document.createElement("name");
    attribute.appendChild(document.createTextNode(name.get()));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("gender");
    attribute.appendChild(document.createTextNode(gender.toString()));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("body");
    attribute.appendChild(document.createTextNode("" + body));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("hair_style");
    attribute.appendChild(document.createTextNode("" + hairStyle));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("hair_color");
    attribute.appendChild(document.createTextNode(hairColor.get().toString()
                                               .substring(2, 8).toUpperCase()));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("eyes");
    attribute.appendChild(document.createTextNode("" + eyes));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("accessory1");
    attribute.appendChild(document.createTextNode("" + accessory1));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("accessory2");
    attribute.appendChild(document.createTextNode("" + accessory2));
    kamui.appendChild(attribute);
    
    attribute = document.createElement("voice");
    attribute.appendChild(document.createTextNode("" + voice));
    kamui.appendChild(attribute);
    
    Set<String> keys = childHairColors.keySet();
    Attr target;
    for (String key : keys) {
      attribute = document.createElement("child_hair_color");
      attribute.appendChild(document.createTextNode(childHairColors.get(key)));
      target = document.createAttribute("target");
      target.setValue(key);
      attribute.getAttributes().setNamedItem(target);
      kamui.appendChild(attribute);
    }
    return kamui;
  }
}
