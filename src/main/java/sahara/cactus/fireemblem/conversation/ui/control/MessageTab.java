/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.ui.control;

import java.util.Optional;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TextInputDialog;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;
import sahara.cactus.fireemblem.conversation.wrapper.FireEmblemMessage;

/**
 * A tab that contains a MessageArea for editing a message within a Fire Emblem
 * text file.
 * 
 * @author Secretive Cactus
 */
public class MessageTab extends Tab {
  
  /**
   * The context menu item for adding a new tab.
   */
  private final MenuItem addTabItem;
  
  /**
   * The context menu item for deleting the tab.
   */
  private final MenuItem deleteTabItem;
  
  /**
   * The context menu item for renaming the tab.
   */
  private final MenuItem renameTabItem;
  
  /**
   * Creates a new MessageTab with the given name, content, and content binding.
   * 
   * @param name the name of the message contained in the tab
   * @param content the content of the message contained in the tab
   * @param binding a binding for the content contained in the tab
   */
  public MessageTab(String name, String content, StringProperty binding) {
    super(name);
    if (Config.getAutoconvertToScript()) {
      content = ScriptHelper.convertToScript(content);
    }
    MessageArea textArea = new MessageArea(content);
    textArea.setWrapText(true);
    binding.bind(textArea.textProperty());
    setContent(textArea);
    ContextMenu contextMenu = new ContextMenu();
    addTabItem = new MenuItem(XMLManager.getText("Message_Tab_Add"));
    addTabItem.textProperty().bind(XMLManager.getTextProperty
                                                           ("Message_Tab_Add"));
    renameTabItem = new MenuItem(XMLManager.getText("Message_Tab_Rename"));
    renameTabItem.textProperty().bind(XMLManager.getTextProperty
                                                        ("Message_Tab_Rename"));
    deleteTabItem = new MenuItem(XMLManager.getText("Message_Tab_Delete"));
    deleteTabItem.textProperty().bind(XMLManager.getTextProperty
                                                        ("Message_Tab_Delete"));
    addTabItem.setOnAction((e) -> add());
    renameTabItem.setOnAction((e) -> rename());
    deleteTabItem.setOnAction((e) -> delete());
    contextMenu.getItems().addAll(addTabItem, renameTabItem, deleteTabItem);
    setContextMenu(contextMenu);
  }
  
  /**
   * Adds a new message key to the current conversation as well as a new tab to
   * the set of MessageTabs.
   */
  private void add() {
    TextInputDialog addDialog = new TextInputDialog(
                  XMLManager.getText("Message_Tab_Add_Dialog_Default_Name"));
    addDialog.setTitle(XMLManager.getText("Message_Tab_Add_Dialog_Title"));
    addDialog.setHeaderText(XMLManager.getText(
                                              "Message_Tab_Add_Dialog_Header"));
    Optional<String> result = addDialog.showAndWait();
    if (result.isPresent()) {
      if (Config.getCurrentConversation().containsMessage(result.get())) {
        Alert alert = new Alert(Alert.AlertType.WARNING,
            XMLManager.getText("Message_Tab_Dialog_Message_Exists_Error"),
                               XMLManager.getButtonType(ButtonData.OK_DONE));
        alert.showAndWait();
      } else {
        FireEmblemMessage message = new FireEmblemMessage(result.get());
        Config.getCurrentConversation().getMessages().add(message);
        getTabPane().getTabs().add(new MessageTab(result.get(), "",
                                                  message.contentProperty()));
      }
    }
  }
  
  /**
   * Deletes this tab and its associated message.
   */
  private void delete() {
    Alert deleteDialog = new Alert(Alert.AlertType.WARNING,
                  XMLManager.getText("Message_Tab_Delete_Dialog_Warning"),
                                     XMLManager.getButtonType(ButtonData.YES),
                                     XMLManager.getButtonType(ButtonData.NO));
    deleteDialog.setTitle(XMLManager.getText(
                                            "Message_Tab_Delete_Dialog_Title"));
    deleteDialog.setHeaderText(XMLManager.getText(
                                           "Message_Tab_Delete_Dialog_Header"));
    Optional<ButtonType> result = deleteDialog.showAndWait();
    if (result.isPresent() && result.get().getButtonData() == ButtonData.YES) {
      Config.getCurrentConversation().removeMessage(
                                          getTabPane().getTabs().indexOf(this));
      removeBindings();
      getTabPane().getTabs().remove(this);
    }
  }
  
  /**
   * Removes the bindings on the tab.
   */
  public void removeBindings() {
    addTabItem.textProperty().unbind();
    renameTabItem.textProperty().unbind();
    deleteTabItem.textProperty().unbind();
  }
  
  /**
   * Renames the tab and its associated message.
   */
  private void rename() {
    TextInputDialog renameDialog = new TextInputDialog(getText());
    renameDialog.setTitle(XMLManager.getText(
                                            "Message_Tab_Rename_Dialog_Title"));
    renameDialog.setHeaderText(XMLManager.getText(
                                           "Message_Tab_Rename_Dialog_Header"));
    Optional<String> result = renameDialog.showAndWait();
    if (result.isPresent()) {
      String text = result.get();
      if (!text.equals(getText())) {
        if (Config.getCurrentConversation().containsMessage(text)) {
          Alert alert = new Alert(Alert.AlertType.WARNING,
            XMLManager.getText("Message_Tab_Dialog_Message_Exists_Error"),
                                 XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
        } else {
          Config.getCurrentConversation().renameMessage(
                                    getTabPane().getTabs().indexOf(this), text);
          setText(result.get());
        }
      }
    }
  }
}