/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.manager;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TabPane;
import jfxtras.labs.util.ColorUtils;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.myunit.Kamui;
import sahara.cactus.fireemblem.conversation.myunit.MyUnit;
import sahara.cactus.fireemblem.conversation.myunit.Robin;
import sahara.cactus.fireemblem.conversation.ui.control.ConversationPane;
import sahara.cactus.fireemblem.conversation.ui.control.MessageArea;
import sahara.cactus.fireemblem.conversation.util.io.ByteUtils;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;
import sahara.cactus.fireemblem.conversation.wrapper.CharacterReference;

/**
 * Manages character information, such as audio data, face data, and mappings
 * between English and Japanese names.
 * 
 * @author SecretiveCactus
 */
public final class CharacterManager {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                              ConversationPane.class.getName());
  
  /**
   * Contains the names of all of the characters who have audio clips.
   */
  private static final ObservableList<CharacterReference> CHARACTER_NAMES;
  static {
    CHARACTER_NAMES = FXCollections.observableArrayList(
      (name) -> new Observable[] { name.searchProperty(), name.soundProperty() }
    );
  }
  
  /**
   * Filters the contents of CHARACTER_NAMES into alphabetical order.
   */
  private static final SortedList<CharacterReference>
                                                    SORTED_CHARACTER_REFERENCES;
  static {
    SORTED_CHARACTER_REFERENCES = CHARACTER_NAMES.sorted(
      (CharacterReference n1, CharacterReference n2) -> n1.compareTo(n2)
    );
  }
  
  /**
   * Filters the contents of SORTED_CHARACTER_REFERENCES based on the user's
   * typed search term. Used as the data source for CHARACTER_LIST.
   */
  private static final FilteredList<CharacterReference>
                                                  FILTERED_CHARACTER_REFERENCES;
  static {
    FILTERED_CHARACTER_REFERENCES = SORTED_CHARACTER_REFERENCES.filtered(
      (CharacterReference n) -> n.matchesSearch()
    );
  }
  
  /**
   * Filters the contents of FILTERED_CHARACTER_REFERENCES to narrow it down to
   * characters that have audio files.
   */
  private static final FilteredList<CharacterReference> CHARACTERS_WITH_AUDIO;
  static {
    CHARACTERS_WITH_AUDIO = FILTERED_CHARACTER_REFERENCES.filtered(
      (CharacterReference n) -> n.hasSound()
    );
  }
  
  /**
   * Is bound to a search box's text property, causing the CharacterReference
   * to be marked as needing to be revalidated by its containing
   * ObservableList whenever the search term is changed. This allows quick
   * automatic filtering.
   */
  private static final StringProperty search = new SimpleStringProperty("");
  
  /**
   * Contains a mapping of names to character references, with the names as the
   * keys.
   */
  private static final HashMap<String, CharacterReference>
                                      CHARACTER_REFERENCE_MAP = new HashMap<>();
  
  /**
   * Contains a list of the current game mode's child characters.
   */
  private static final ObservableList<CharacterReference> CHILD_LIST =
                                            FXCollections.observableArrayList();
  
  /**
   * Contains a mapping of names to face data, with the names as the keys.
   */
  private static final HashMap<String, byte[]> FACE_DATA = new HashMap<>();
  
  /**
   * Contains a mapping between the character's English and Japanese names, with
   * the Japanese names as the keys.
   */
  private static final HashMap<String, String> NAME_REFERENCE = new HashMap<>();
  
  /**
   * Contains a mapping between the character's English and Japanese names, with
   * the English names as the keys.
   */
  private static final HashMap<String, String> INVERSE_NAME_REFERENCE =
                                                                new HashMap<>();
  
  /**
   * Contains the current My Unit.
   */
  public static final ObjectProperty<MyUnit> myUnit =
                                                   new SimpleObjectProperty<>();
  
  /**
   * Contains all of the My Units for the current loaded game type.
   */
  public static final ObservableList<MyUnit> MY_UNITS =
                FXCollections.observableArrayList((myUnit) -> new Observable[] {
                                                    myUnit.nameProperty()
                                                  });
  
  /**
   * The name of the last My Unit, used when swapping names in script mode.
   */
  private static String LAST_MY_UNIT_NAME;
  
  /**
   * The tabs in which name swapping should be performed.
   */
  public static TabPane MESSAGE_TABS;
  
  /**
   * Listens for changes to the name of the current My Unit and updates the
   * name references accordingly.
   */
  private static class NameListener implements ChangeListener<String> {
    
    /**
     * Watches for changes in the name of the currently active My Unit and
     * rewrites script commands as appropriate, if the option to do so is
     * enabled. My Units who share names with characters already present in the
     * current game mode do not qualify for this feature, as the existing
     * character takes priority.
     * 
     * @param o the name property of the My Unit
     * @param oV the old value of the My Unit's name property
     * @param nV the new value of the My Unit's name property
     */
    @Override
    public void changed(ObservableValue o, String oV, String nV) {
      if (!Config.getUseMyUnitNameInScripts() || nV.equals(oV)) {
        return;
      }
      if ("username".equals(INVERSE_NAME_REFERENCE.get(oV))) {
        INVERSE_NAME_REFERENCE.remove(oV);
      }
      if (INVERSE_NAME_REFERENCE.get(nV) == null) {
        LAST_MY_UNIT_NAME = nV;
        NAME_REFERENCE.put("username", nV);
        INVERSE_NAME_REFERENCE.put(nV, "username");
        if (MESSAGE_TABS != null) {
          MESSAGE_TABS.getTabs().forEach((t) -> {
            MessageArea area = ((MessageArea) t.getContent());
            String text = area.getText();
            if (text.startsWith("[" +
                                XMLManager.getText("Script_Command_Script")) ||
                text.startsWith("[" +
                       XMLManager.getText("Script_Command_Formatted_Script"))) {
              int pos = area.getCaretPosition();
              area.replaceText(text.replaceAll(Pattern.quote(oV),
                                               Matcher.quoteReplacement(nV)));
              area.moveTo(pos);
            }
          });
        }
      } else {
        LAST_MY_UNIT_NAME = XMLManager.getText("Default_My_Unit_Name");
        NAME_REFERENCE.put("username", LAST_MY_UNIT_NAME);
        INVERSE_NAME_REFERENCE.put(LAST_MY_UNIT_NAME, "username");
        if (MESSAGE_TABS != null) {
          MESSAGE_TABS.getTabs().forEach((t) -> {
            MessageArea area = ((MessageArea) t.getContent());
            String text = area.getText();
            if (text.startsWith("[" +
                                XMLManager.getText("Script_Command_Script")) ||
                text.startsWith("[" +
                       XMLManager.getText("Script_Command_Formatted_Script"))) {
              int pos = area.getCaretPosition();
              area.replaceText(text.replaceAll(Pattern.quote(oV),
                                  Matcher.quoteReplacement(LAST_MY_UNIT_NAME)));
              area.moveTo(pos);
            }
          });
        }
      }
    }
  }
  private static final NameListener NAME_LISTENER = new NameListener();
  static {
    Config.useMyUnitNameInScriptsProperty().addListener((o, oV, nV) -> {
      if (nV) {
        myUnit.get().nameProperty().addListener(NAME_LISTENER);
        NAME_LISTENER.changed(myUnit.get().nameProperty(),
                              LAST_MY_UNIT_NAME, myUnit.get().getName());
      } else {
        INVERSE_NAME_REFERENCE.remove(myUnit.get().getName());
        LAST_MY_UNIT_NAME = XMLManager.getText("Default_My_Unit_Name");
        myUnit.get().nameProperty().removeListener(NAME_LISTENER);
        NAME_REFERENCE.put("username", LAST_MY_UNIT_NAME);
        INVERSE_NAME_REFERENCE.put(LAST_MY_UNIT_NAME, "username");
        if (MESSAGE_TABS != null) {
          MESSAGE_TABS.getTabs().forEach((t) -> {
            MessageArea area = ((MessageArea) t.getContent());
            String text = area.getText();
            if (text.startsWith("[" +
                                XMLManager.getText("Script_Command_Script")) ||
                text.startsWith("[" +
                       XMLManager.getText("Script_Command_Formatted_Script"))) {
              int pos = area.getCaretPosition();
              area.replaceText(text.replaceAll(
                                 Pattern.quote(myUnit.get().getName()),
                                  Matcher.quoteReplacement(LAST_MY_UNIT_NAME)));
              area.moveTo(pos);
            }
          });
        }
      }
    });
    myUnit.addListener((o, oV, nV) -> {
      if (oV != null) {
        oV.nameProperty().removeListener(NAME_LISTENER);
        NAME_LISTENER.changed(nV.nameProperty(), oV.getName(), nV.getName());
      } else {
        NAME_LISTENER.changed(nV.nameProperty(), "", nV.getName());
      }
      nV.nameProperty().addListener(NAME_LISTENER);
    });
  }
  
  /**
   * Initializes the character manager's contents.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public static void initialize() throws GameBranchMissingException {
    loadCharacters();
    loadMyUnits();
  }
  
  /**
   * Loads the My Units for the current game mode.
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public static void loadMyUnits() throws GameBranchMissingException {
    if (myUnit.get() != null) {
      myUnit.get().nameProperty().removeListener(NAME_LISTENER);
    }
    LAST_MY_UNIT_NAME = XMLManager.getText("Default_My_Unit_Name");
    MY_UNITS.clear();
    XMLManager.loadMyUnits(MY_UNITS);
    if (MY_UNITS.size() == 0) {
      GameMode.branch(() -> {
        MY_UNITS.add(new Robin());
      }, () -> {
        MY_UNITS.add(new Kamui());
      });
    }
    Integer i = Config.MY_UNIT_INDICES.get(Config.getGameMode());
    if (i == null) {
      i = 0;
    }
    myUnit.set(MY_UNITS.get(i));
  }
  
  /**
   * Returns the filtered list of characters that have audio files.
   * 
   * @return the filtered list of characters that have audio files
   */
  public static FilteredList<CharacterReference> charactersWithAudio() {
    return CHARACTERS_WITH_AUDIO;
  }
  
  /**
   * Returns the list of characters filtered based on the user's search term.
   * 
   * @return the list of characters filtered based on the user's search term
   */
  public static FilteredList<CharacterReference> getFilteredCharacters() {
    return FILTERED_CHARACTER_REFERENCES;
  }
  
  /**
   * Gets the reference for the character with the given name.
   * 
   * @param name the character name for which a reference should be returned
   * @return the reference for the character with the given name
   */
  public static CharacterReference getByName(String name) {
    return CHARACTER_REFERENCE_MAP.get(name);
  }
  
  /**
   * Gets the list of references for the current game's child characters.
   * 
   * @return the list of references for the current game's child characters
   */
  public static ObservableList<CharacterReference> getChildren() {
    return CHILD_LIST;
  }
  
  /**
   * Gets the face data for the character with the given name.
   * 
   * @param name the character for which face data should be returned
   * @return the face data for the character with the given name
   */
  public static byte[] getFaceData(String name) {
    return FACE_DATA.get(name);
  }
  
  /**
   * Returns a mapping between the translated and Japanese names of every
   * character in the loaded game, with the translated names as the keys.
   * 
   * @return a mapping between the translated and Japanese names of every
   * character in the loaded game, with the translated names as the keys
   */
  public static HashMap<String,String> getInverseNames() {
    return INVERSE_NAME_REFERENCE;
  }
  
  
  /**
   * Gets the Japanese name for the specified character.
   * 
   * @param name the character's translated name
   * @return the character's Japanese name
   */
  public static String getJapaneseName(String name) {
    String result = INVERSE_NAME_REFERENCE.get(name);
    if (result != null) {
      return result;
    } else if (name.equals(myUnit.get().getName())) {
      return "username";
    }
    return name;
  }
  
  /**
   * Returns the active My Unit.
   * 
   * @return the active My Unit
   */
  public static MyUnit getMyUnit() {
    return myUnit.get();
  }
  
  /**
   * Returns a mapping between the translated and Japanese names of every
   * character in the loaded game, with the Japanese names as the keys.
   * 
   * @return a mapping between the translated and Japanese names of every
   * character in the loaded game, with the Japanese names as the keys
   */
  public static HashMap<String,String> getNames() {
    return NAME_REFERENCE;
  }
  
  /**
   * Gets the translated name for the specified character.
   * 
   * @param name the character's Japanese name
   * @return the character's translated name
   */
  public static String getTranslatedName(String name) {
    String result = NAME_REFERENCE.get(name);
    if (result != null) {
      return result;
    }
    return name;
  }
  
  /**
   * Loads all of the data related to the characters for the current game.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public static void loadCharacters() throws GameBranchMissingException {
    CHARACTER_NAMES.forEach((CharacterReference c) -> c.erase());
    CHARACTER_NAMES.clear();
    NAME_REFERENCE.clear();
    INVERSE_NAME_REFERENCE.clear();
    CHILD_LIST.clear();
    String myUnitName = XMLManager.getText("Default_My_Unit_Name");
    NAME_REFERENCE.put("username", myUnitName);
    INVERSE_NAME_REFERENCE.put(myUnitName, "username");
    XMLManager.loadXMLAsStrings("PID", NAME_REFERENCE);
    HashMap<String,String> soundRefs = new HashMap<>();
    XMLManager.loadXMLAsStrings(ResourceManager.getFileResource(
                                        "txt/xml/system/Voice.xml"), soundRefs);
    HashMap<String,String> hairRefs = new HashMap<>();
    XMLManager.loadXMLAsStrings(ResourceManager.getFileResource(
                                          "txt/xml/system/Hair.xml"), hairRefs);
    CharacterReference c;
    String name;
    String hexColor;
    for (String key : NAME_REFERENCE.keySet()) {
      name = NAME_REFERENCE.get(key);
      c = new CharacterReference(name, key);
      c.searchProperty().bind(search);
      c.setSoundName(soundRefs.get(key));
      if ((hexColor = hairRefs.get(name)) != null) {
        c.setHairColor(ColorUtils.webColorToColor(hexColor));
      }
      CHARACTER_REFERENCE_MAP.put(name, c);
      CHARACTER_NAMES.add(c);
      INVERSE_NAME_REFERENCE.put(name, key);
    }
    ArrayList<String> children = XMLManager.getTextNodesOfType(
                 ResourceManager.getFileResource("txt/xml/system/Children.xml"),
                                                               "child");
    for (String child : children) {
      CHILD_LIST.add(CHARACTER_REFERENCE_MAP.get(NAME_REFERENCE.get(child)));
    }
    loadFaces();
    ScriptHelper.reloadUniqueEmotions();
  }
  
  /**
   * Loads the face data for all of the characters in the current game.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  private static void loadFaces() throws GameBranchMissingException {
    FACE_DATA.clear();
    int size = 0;
    UnaryOperator<Integer> awakening = (i) -> 0x28;
    UnaryOperator<Integer> fates = (i) -> 0x48;
    size = GameMode.branch(size, awakening, fates);
    byte[] faces = ByteUtils.getFileBytes(
                              ResourceManager.getFileResource("bin/faces.bin"));
    FileInputStream fis;
    InputStreamReader isr;
    BufferedReader br;
    byte[] dat;
    try {
      fis = new FileInputStream(ResourceManager.getFileResource("txt/FID.txt"));
      isr = new InputStreamReader(fis, "UTF-8");
      br = new BufferedReader(isr);
      int i = 0;
      String line;
      while ((line = br.readLine()) != null) {
        if (line.isEmpty()) {
          continue;
        }
        dat = new byte[size];
        System.arraycopy(faces, i * size, dat, 0, size);
        FACE_DATA.put(line, dat);
        i++;
      }
      br.close();
      isr.close();
      fis.close();
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }
  
  /**
   * Returns the character manager's My Unit property.
   * 
   * @return the character manager's My Unit property
   */
  public static ObjectProperty<MyUnit> myUnitProperty() {
    return myUnit;
  }
  
  /**
   * Returns the character managers's search property.
   * 
   * @return the character manager's search property
   */
  public static StringProperty searchProperty() {
    return search;
  }
  
  /**
   * Sets the active My Unit.
   * 
   * @param newUnit the newly active My Unit
   */
  public static void setMyUnit(MyUnit newUnit) {
    myUnit.set(newUnit);
  }
}
