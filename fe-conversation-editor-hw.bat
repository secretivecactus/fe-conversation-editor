@ECHO OFF
IF EXIST "target\fe-conversation-editor.jar" (
  java -XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -Xms64m -Xmx512m -jar target/fe-conversation-editor.jar %*
  BREAK
) ELSE (
  IF EXIST "bin\fe-conversation-editor.jar" (
    java -XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -Xms64m -Xmx512m -jar bin/fe-conversation-editor.jar %*
    BREAK
  ) ELSE (
    IF EXIST "fe-conversation-editor.jar" (
      IF NOT EXIST "bin" MKDIR "bin"
      MOVE "fe-conversation-editor.jar" "bin/fe-conversation-editor.jar"
      java -XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -Xms64m -Xmx512m -jar fe-conversation-editor.jar %*
      BREAK
    )
  )
)
